# PyTorch MNIST

I was learning how to use PyTorch with MNIST example.
Basiclly, I just copy the code on [yunjey's pytorch tutorial](https://github.com/yunjey/pytorch-tutorial/blob/master/tutorials/02-intermediate/convolutional_neural_network/main.py#L35-L56) and made some modifications of it.

# Run
```
    # make sure pytorch and torchvision is installed.
    $ python3 main.py
```

## mnist_model_99.31.pth

This file contains the model I've trainned.

**Model Structure**
```
MNISTClassifier(                                                                    
  (cnn): Sequential(
    (0): Conv2d(1, 32, kernel_size=(8, 8), stride=(1, 1), padding=(2, 2))
    (1): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (2): ReLU()
    (3): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
    (4): Conv2d(32, 64, kernel_size=(4, 4), stride=(1, 1), padding=(2, 2))
    (5): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
    (6): ReLU()
    (7): MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False)
  )
  (hidden_dense1): Sequential(
    (0): Linear(in_features=2304, out_features=128, bias=True)
    (1): ReLU()
    (2): Dropout(p=0.25)
  )
  (hidden_dense2): Sequential(
    (0): Linear(in_features=128, out_features=128, bias=True)
    (1): ReLU()
    (2): Dropout(p=0.25)
  )
  (out_dense): Linear(in_features=128, out_features=10, bias=True)
)                                                                                  
```

**Accuracy: 99.31%**

Learning rate: 0.001

Epochs trainned: 10

Steps per epoch: 1200

Batch size: 50

Device: My laptop (Core i5-6200U CPU, 8GB of RAM)

My laptop's GPU didn't support CUDA, so I have to train it on my CPU...


## Useful links

[https://github.com/yunjey/pytorch-tutorial](https://github.com/yunjey/pytorch-tutorial)
