import torch
import torch.nn as nn
import torch.optim as optim
import torch.utils.data as data
import torchvision

import os

EPOCH = 10
BATCH_SIZE = 50
LEARNING_RATE = 0.001

DATASET_PATH = './mnist/'
MODEL_FILE = 'mnist_model.pth'

#choose which device to compute
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

class MNISTClassifier(nn.Module):
    def __init__(self):
        super(MNISTClassifier, self).__init__()

        self.cnn = nn.Sequential(
            #first layer
            nn.Conv2d(in_channels=1, out_channels=32, kernel_size=8, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2),
            #second layer
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=4, padding=2),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(kernel_size=2)
        )
        self.hidden_dense1 = nn.Sequential(
            nn.Linear(in_features=64 * 6 * 6, out_features=128),
            nn.ReLU(),
            nn.Dropout(0.25)
        )
        self.hidden_dense2 = nn.Sequential(
            nn.Linear(in_features=128, out_features=128),
            nn.ReLU(),
            nn.Dropout(0.25)
        )
        self.out_dense = nn.Linear(in_features=128, out_features=10)

    def forward(self, image):
        out = self.cnn(image)
        # flatten the cnn outputs
        out = out.reshape(out.size(0), -1)
        out = self.hidden_dense1(out)
        out = self.hidden_dense2(out)
        return self.out_dense(out)

def main():
    download_dataset = False
    if not os.path.exists(DATASET_PATH) or not os.listdir(DATASET_PATH):
        #if we don't have the dataset
        download_dataset = True
    #download dataset if we don't have one
    train_data = torchvision.datasets.MNIST(DATASET_PATH, train=True, transform=torchvision.transforms.ToTensor(), download=download_dataset)
    
    #create a data loader
    data_loader= data.DataLoader(train_data, BATCH_SIZE, shuffle=True)

    #create our MNIST classifier
    model = MNISTClassifier()
    #print out structure of our model
    print(model)

    optimizer = optim.Adam(model.parameters(), lr=LEARNING_RATE)
    loss_func = nn.CrossEntropyLoss()

    print('Tranning...')
    for epoch in range(EPOCH):
        for step, (x, y) in enumerate(data_loader):
            # transfer data and images if needed
            x = x.to(device)
            y = y.to(device)
            #this will call function 'forward' in our model
            prediction = model(x)
            #calculate the loss of the model
            loss = loss_func(prediction, y)
            #reset gradient
            optimizer.zero_grad()
            #do backpropagation
            loss.backward()
            #apply change to weights
            optimizer.step()
            
            #logging
            if step % 100 == 0:
                print('Epoch: {}/{} | Step: {}/{} | Loss: {}'
                    .format(epoch+1, EPOCH, step, len(data_loader), loss.item()))
    
    #do testing with test data
    print('Testing...')
    #prepare test data
    test_data = torchvision.datasets.MNIST(DATASET_PATH, False, transform=torchvision.transforms.ToTensor(), download=download_dataset)
    data_loader = data.DataLoader(dataset=test_data, batch_size=BATCH_SIZE, shuffle=True)
    #turn model into evaluation mode
    model.eval()
    #disable gradient calculation
    with torch.no_grad():
        correct = 0
        total = 0
        for x, y in data_loader:
            x = x.to(device)
            y = y.to(device)
            #do feed forward
            outputs = model(x)
            #one-hot the output of the model
            _, prediction = torch.max(outputs.data, 1)
            total += y.size(0)
            correct += (prediction == y).sum().item()
        #calculate percentage of accuracy
        acc = float(correct / total) * float(100)
        print('Test acc: {}%'.format(acc))
    # if our model got more than 98% accuracy
    if acc > 98:
        #save model
        print('Saving model...')
        torch.save(model.state_dict(), 'mnist_model_{}.pth'.format(acc))
    else:
        print('The model didn\'t met the accuracy to be saved.')

if __name__ == '__main__':
    print('CUDA available: {}'.format('True' if torch.cuda.is_available() else 'False'))
    main()